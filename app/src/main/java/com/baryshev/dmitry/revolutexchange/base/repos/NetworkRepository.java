package com.baryshev.dmitry.revolutexchange.base.repos;

import android.support.annotation.NonNull;

import com.baryshev.dmitry.revolutexchange.network.ApiService;
import com.baryshev.dmitry.revolutexchange.network.pojo.BaseRate;

import java.io.IOException;

import io.reactivex.Flowable;

public class NetworkRepository implements INetworkRepository {

    @NonNull
    @Override
    public Flowable<BaseRate> getBaseRate(@NonNull String base) {
        return ApiService.getInstance()
                         .getApi()
                         .getBaseRate(base)
                         .switchIfEmpty(Flowable.error(new IOException()));
    }
}
