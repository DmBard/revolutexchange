package com.baryshev.dmitry.revolutexchange.exchange.view;

import android.support.annotation.NonNull;

import com.baryshev.dmitry.revolutexchange.exchange.model.ExchangeRateVM;

public interface IExchangeView {

    IExchangeView EMPTY = new IExchangeView() {
        @Override
        public void updateToRate(@NonNull ExchangeRateVM rateVM) {

        }

        @Override
        public void updateFromRate(@NonNull ExchangeRateVM rateVM) {

        }

        @Override
        public void showError(int errorMessage) {

        }

        @Override
        public void updateExchangeKoeff(@NonNull ExchangeRateVM exchangeRateVM) {

        }
    };

    void updateToRate(@NonNull ExchangeRateVM rateVM);

    void updateFromRate(@NonNull ExchangeRateVM rateVM);

    void showError(int errorMessage);

    void updateExchangeKoeff(@NonNull ExchangeRateVM exchangeRateVM);
}
