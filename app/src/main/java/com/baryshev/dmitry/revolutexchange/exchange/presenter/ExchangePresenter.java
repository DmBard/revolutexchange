package com.baryshev.dmitry.revolutexchange.exchange.presenter;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.baryshev.dmitry.revolutexchange.base.CurrencyType;
import com.baryshev.dmitry.revolutexchange.base.ErrorType;
import com.baryshev.dmitry.revolutexchange.base.ISchedulerManager;
import com.baryshev.dmitry.revolutexchange.exchange.model.ExchangeRateVM;
import com.baryshev.dmitry.revolutexchange.exchange.model.IExchangeInteractor;
import com.baryshev.dmitry.revolutexchange.exchange.view.IExchangeView;
import com.baryshev.dmitry.revolutexchange.network.pojo.Rates;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class ExchangePresenter {
    private static final int UPDATE_INTERVAL = 30; // seconds

    @NonNull
    private final IExchangeInteractor exchangeInteractor;
    @NonNull
    private final ISchedulerManager schedulerManager;
    @SuppressWarnings("NullableProblems")
    @NonNull
    IExchangeView view;
    @NonNull
    ExchangeRateVM rateVM = new ExchangeRateVM();
    @NonNull
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Nullable
    private Disposable updateRatesDisposable;

    public ExchangePresenter(@NonNull IExchangeInteractor exchangeInteractor,
                             @NonNull ISchedulerManager schedulerManager) {

        this.exchangeInteractor = exchangeInteractor;
        this.schedulerManager = schedulerManager;
    }

    public void onRateValueChanged(@Nullable String rateVal,
                                   @ExchangeDirection int exchangeDirection) {
        NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
        try {
            Number number = numberFormat.parse(rateVal);
            switch (exchangeDirection) {
                case ExchangeDirection.TO:
                    rateVM.setToVal(number.doubleValue());
                    break;
                case ExchangeDirection.FROM:
                    rateVM.setFromVal(number.doubleValue());
                    break;
            }
            updateRates(exchangeDirection,
                        exchangeInteractor.getRates(rateVM.getFromCurrencyType().name()),
                        true);
        } catch (ParseException e) {
            switch (exchangeDirection) {
                case ExchangeDirection.TO:
                    view.updateToRate(rateVM);
                    break;
                case ExchangeDirection.FROM:
                    view.updateFromRate(rateVM);
                    break;
            }
        }
    }

    private void updateRates(@ExchangeDirection int exchangeDirection,
                             @NonNull Flowable<Rates> ratesFlowable,
                             boolean isNeedToDisposePreviously) {
        if (updateRatesDisposable != null && isNeedToDisposePreviously) {
            updateRatesDisposable.dispose();
        }
        Disposable disposable = ratesFlowable.map(this::calcRateKoeff)
                .doOnNext(koeff -> rateVM.setRateKoeff(koeff))
                .map(koeff -> calcRatesByKoeff(exchangeDirection,
                                               koeff))
                .subscribeOn(schedulerManager.io())
                .observeOn(schedulerManager.mainThread())
                .subscribe(exchangeRateVM -> {
                    switch (exchangeDirection) {
                        case ExchangeDirection.FROM:
                            view.updateToRate(exchangeRateVM);
                            break;
                        case ExchangeDirection.TO:
                            view.updateFromRate(exchangeRateVM);
                            break;
                    }
                    view.updateExchangeKoeff(exchangeRateVM);
                }, this::handleError);
        if (isNeedToDisposePreviously) {
            updateRatesDisposable = disposable;
        }
        compositeDisposable.add(disposable);
    }

    @NonNull
    private ExchangeRateVM calcRatesByKoeff(@ExchangeDirection int exchangeDirection,
                                            double koeff) {
        switch (exchangeDirection) {
            case ExchangeDirection.FROM:
                rateVM.setToVal(rateVM.getFromVal() * koeff);
                break;
            case ExchangeDirection.TO:
                rateVM.setFromVal(rateVM.getToVal() / koeff);
                break;
        }
        return rateVM;
    }

    private void handleError(@NonNull Throwable throwable) {
        if (throwable instanceof UnknownHostException) {
            view.showError(ErrorType.NETWORK.getErrorMessage());
        } else if (throwable instanceof IOException) {
            view.showError(ErrorType.COMMON.getErrorMessage());
        }

    }

    public void onRateTypeChanged(@NonNull CurrencyType currencyType,
                                  @ExchangeDirection int exchangeDirection) {
        switch (exchangeDirection) {
            case ExchangeDirection.TO:
                rateVM.setToCurrencyType(currencyType);
                break;
            case ExchangeDirection.FROM:
                rateVM.setFromCurrencyType(currencyType);
                break;
        }
        updateRates(ExchangeDirection.FROM,
                    exchangeInteractor.getRates(rateVM.getFromCurrencyType().name()),
                    true);
    }

    private double calcRateKoeff(Rates rates) {
        double koeff = 1;
        switch (rateVM.getToCurrencyType()) {
            case EUR:
                koeff = rates.getEur();
                break;
            case GBP:
                koeff = rates.getGbp();
                break;
            case USD:
                koeff = rates.getUsd();
                break;
        }
        return koeff == 0 ? 1 : koeff;
    }

    public void onDetach() {
        view = IExchangeView.EMPTY;
        compositeDisposable.clear();
    }

    public void onInit(@NonNull IExchangeView view,
                       @NonNull CurrencyType fromCurrencyType,
                       @NonNull CurrencyType toCurrencyType) {
        compositeDisposable.clear();

        this.view = view;

        onUpdateRates(fromCurrencyType, toCurrencyType);
    }

    public void onUpdateRates(@NonNull CurrencyType fromCurrencyType,
                               @NonNull CurrencyType toCurrencyType) {
        rateVM.setFromCurrencyType(fromCurrencyType);
        rateVM.setToCurrencyType(toCurrencyType);
        updateRates(ExchangeDirection.FROM,
                    Flowable.interval(0, UPDATE_INTERVAL, TimeUnit.SECONDS, schedulerManager.io())
                            .flatMap(time -> exchangeInteractor.getRates(rateVM.getFromCurrencyType()
                                                                                 .name())),
                    false);
    }

    public void onFocusChanged() {
        view.updateExchangeKoeff(rateVM);
    }

    @IntDef({ExchangeDirection.FROM, ExchangeDirection.TO})
    public @interface ExchangeDirection {
        int FROM = 0;
        int TO = 1;
    }
}
