package com.baryshev.dmitry.revolutexchange.exchange.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.baryshev.dmitry.revolutexchange.R;
import com.baryshev.dmitry.revolutexchange.base.CurrencyType;
import com.baryshev.dmitry.revolutexchange.base.SchedulerManager;
import com.baryshev.dmitry.revolutexchange.base.repos.NetworkRepository;
import com.baryshev.dmitry.revolutexchange.exchange.model.ExchangeInteractor;
import com.baryshev.dmitry.revolutexchange.exchange.model.ExchangeRateVM;
import com.baryshev.dmitry.revolutexchange.exchange.presenter.ExchangePresenter;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements IExchangeView {

    @SuppressWarnings("NullableProblems")
    @NonNull
    private ExchangePresenter presenter;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private EditText etRateFrom;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private EditText etRateTo;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private TextWatcher rateToWatcher;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private TextWatcher rateFromWatcher;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private TextView tvRateKoeff;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private TextView tvFromTo;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private TextView tvToFrom;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private Spinner spRateFrom;
    @SuppressWarnings("NullableProblems")
    @NonNull
    private Spinner spRateTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initEditText();
        initSpinners();

        tvRateKoeff = (TextView) findViewById(R.id.tv_rate_koeff);
        tvFromTo = (TextView) findViewById(R.id.tv_rate_from_to);
        tvToFrom = (TextView) findViewById(R.id.tv_rate_to_from);

        presenter = new ExchangePresenter(new ExchangeInteractor(new NetworkRepository()),
                                          new SchedulerManager());
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onInit(this,
                         (CurrencyType) spRateFrom.getSelectedItem(),
                         (CurrencyType) spRateTo.getSelectedItem());
    }

    @Override
    protected void onStop() {
        presenter.onDetach();
        super.onStop();
    }

    private void initEditText() {
        etRateFrom = (EditText) findViewById(R.id.et_rate_from);
        rateFromWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.onRateValueChanged(editable.toString(),
                                             ExchangePresenter.ExchangeDirection.FROM);
            }
        };
        etRateFrom.addTextChangedListener(rateFromWatcher);
        etRateFrom.setOnFocusChangeListener((view, isFocused) -> {
            if (isFocused) {
                presenter.onFocusChanged();
            }
        });

        etRateTo = (EditText) findViewById(R.id.et_rate_to);
        rateToWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                presenter.onRateValueChanged(editable.toString(),
                                             ExchangePresenter.ExchangeDirection.TO);
            }
        };

        etRateTo.addTextChangedListener(rateToWatcher);
        etRateTo.setOnFocusChangeListener((view, isFocused) -> {
            if (isFocused) {
                presenter.onFocusChanged();
            }
        });
    }

    private void initSpinners() {
        spRateFrom = (Spinner) findViewById(R.id.sp_rate_from);
        spRateTo = (Spinner) findViewById(R.id.sp_rate_to);

        ArrayAdapter<CurrencyType> adapter = new ArrayAdapter<>(this,
                                                                R.layout.spinner_view,
                                                                CurrencyType.values());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spRateFrom.setAdapter(adapter);
        spRateFrom.setSelection(CurrencyType.EUR.ordinal());
        spRateFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                presenter.onRateTypeChanged((CurrencyType) adapterView.getSelectedItem(),
                                            ExchangePresenter.ExchangeDirection.FROM);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spRateTo.setAdapter(adapter);
        spRateTo.setSelection(CurrencyType.USD.ordinal());
        spRateTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                presenter.onRateTypeChanged((CurrencyType) adapterView.getSelectedItem(),
                                            ExchangePresenter.ExchangeDirection.TO);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void updateToRate(@NonNull ExchangeRateVM rateVM) {
        updateRateField(etRateTo, rateToWatcher, String.format(Locale.getDefault(),
                                                               rateVM.getToVal() == 0 ? "%.0f" : "%.2f",
                                                               rateVM.getToVal()));
    }

    private void updateRateField(@NonNull EditText editText,
                                 @NonNull TextWatcher textWatcher,
                                 @NonNull String text) {
        editText.removeTextChangedListener(textWatcher);
        editText.setText(text);
        editText.addTextChangedListener(textWatcher);
    }

    @Override
    public void updateFromRate(@NonNull ExchangeRateVM rateVM) {
        updateRateField(etRateFrom, rateFromWatcher, String.format(Locale.getDefault(),
                                                                   rateVM.getFromVal() == 0 ? "%s" : "%.2f",
                                                                   rateVM.getFromVal()));
    }

    @Override
    public void showError(int errorMessage) {
        Snackbar.make(findViewById(R.id.root),
                      errorMessage,
                      BaseTransientBottomBar.LENGTH_INDEFINITE)
                .setActionTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setAction(R.string.retry,
                           view1 -> presenter.onUpdateRates((CurrencyType) spRateFrom.getSelectedItem(),
                                                            (CurrencyType) spRateTo.getSelectedItem()))
                .show();
    }

    @Override
    public void updateExchangeKoeff(@NonNull ExchangeRateVM exchangeRateVM) {
        CurrencyType fromCurrencyType = exchangeRateVM.getFromCurrencyType();
        CurrencyType toCurrencyType = exchangeRateVM.getToCurrencyType();
        if (fromCurrencyType.equals(toCurrencyType)) {
            tvRateKoeff.setVisibility(View.INVISIBLE);
            tvFromTo.setVisibility(View.INVISIBLE);
            tvToFrom.setVisibility(View.INVISIBLE);
            return;
        }

        tvRateKoeff.setVisibility(View.VISIBLE);

        double rateKoeff = exchangeRateVM.getRateKoeff();
        String fromTo = String.format(Locale.getDefault(),
                                      "%s1 = %s%.3f",
                                      fromCurrencyType.getCurrency(),
                                      toCurrencyType.getCurrency(),
                                      rateKoeff);
        String toFrom = String.format(Locale.getDefault(),
                                      "%s1 = %s%.3f",
                                      toCurrencyType.getCurrency(),
                                      fromCurrencyType.getCurrency(),
                                      1 / rateKoeff);
        if (etRateTo.isFocused()) {
            tvFromTo.setVisibility(View.VISIBLE);
            tvToFrom.setVisibility(View.INVISIBLE);
            tvFromTo.setText(fromTo);
            tvRateKoeff.setText(toFrom);
        } else {
            tvFromTo.setVisibility(View.INVISIBLE);
            tvToFrom.setVisibility(View.VISIBLE);
            tvToFrom.setText(toFrom);
            tvRateKoeff.setText(fromTo);
        }
    }
}
