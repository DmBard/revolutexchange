package com.baryshev.dmitry.revolutexchange.base;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;

public interface ISchedulerManager {
    @NonNull
    Scheduler io();

    @NonNull
    Scheduler mainThread();
}
