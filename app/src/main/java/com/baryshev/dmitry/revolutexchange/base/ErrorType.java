package com.baryshev.dmitry.revolutexchange.base;

import android.support.annotation.StringRes;

import com.baryshev.dmitry.revolutexchange.R;

public enum ErrorType {
    NETWORK(R.string.error_network),
    COMMON(R.string.error_common);

    @StringRes
    private int errorMessage;

    ErrorType(@StringRes int errorMessage) {
        this.errorMessage = errorMessage;
    }

    @StringRes
    public int getErrorMessage() {
        return errorMessage;
    }
}
