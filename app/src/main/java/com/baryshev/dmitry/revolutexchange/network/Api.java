package com.baryshev.dmitry.revolutexchange.network;

import android.support.annotation.NonNull;

import com.baryshev.dmitry.revolutexchange.network.pojo.BaseRate;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    @GET("/latest")
    @NonNull
    Flowable<BaseRate> getBaseRate(@Query("base") @NonNull String base);

}
