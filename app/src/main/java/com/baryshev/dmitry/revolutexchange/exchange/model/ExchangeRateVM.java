package com.baryshev.dmitry.revolutexchange.exchange.model;

import android.support.annotation.NonNull;

import com.baryshev.dmitry.revolutexchange.base.CurrencyType;

public class ExchangeRateVM {
    @NonNull
    private CurrencyType fromCurrencyType = CurrencyType.EUR;
    @NonNull
    private CurrencyType toCurrencyType = CurrencyType.USD;
    private double fromVal = 0;
    private double toVal = 0;
    private double rateKoeff = 1;

    @NonNull
    public CurrencyType getFromCurrencyType() {
        return fromCurrencyType;
    }

    public void setFromCurrencyType(@NonNull CurrencyType fromCurrencyType) {
        this.fromCurrencyType = fromCurrencyType;
    }

    @NonNull
    public CurrencyType getToCurrencyType() {
        return toCurrencyType;
    }

    public void setToCurrencyType(@NonNull CurrencyType toCurrencyType) {
        this.toCurrencyType = toCurrencyType;
    }

    public double getFromVal() {
        return fromVal;
    }

    public void setFromVal(double fromVal) {
        this.fromVal = fromVal;
    }

    public double getToVal() {
        return toVal;
    }

    public void setToVal(double toVal) {
        this.toVal = toVal;
    }

    public double getRateKoeff() {
        return rateKoeff;
    }

    public void setRateKoeff(double rateKoeff) {
        this.rateKoeff = rateKoeff;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = fromCurrencyType.hashCode();
        result = 31 * result + toCurrencyType.hashCode();
        temp = Double.doubleToLongBits(fromVal);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(toVal);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(rateKoeff);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExchangeRateVM that = (ExchangeRateVM) o;

        if (Double.compare(that.fromVal, fromVal) != 0) return false;
        if (Double.compare(that.toVal, toVal) != 0) return false;
        if (Double.compare(that.rateKoeff, rateKoeff) != 0) return false;
        if (fromCurrencyType != that.fromCurrencyType) return false;
        return toCurrencyType == that.toCurrencyType;

    }
}
