package com.baryshev.dmitry.revolutexchange.exchange.model;

import android.support.annotation.NonNull;

import com.baryshev.dmitry.revolutexchange.network.pojo.Rates;

import io.reactivex.Flowable;

public interface IExchangeInteractor {
    @NonNull
    Flowable<Rates> getRates(@NonNull String baseRate);
}
