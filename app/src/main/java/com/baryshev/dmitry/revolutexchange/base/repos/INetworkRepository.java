package com.baryshev.dmitry.revolutexchange.base.repos;

import android.support.annotation.NonNull;

import com.baryshev.dmitry.revolutexchange.network.pojo.BaseRate;

import io.reactivex.Flowable;

public interface INetworkRepository {
    @NonNull
    Flowable<BaseRate> getBaseRate(@NonNull String base);
}
