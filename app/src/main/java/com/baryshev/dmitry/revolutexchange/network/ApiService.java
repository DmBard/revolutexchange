package com.baryshev.dmitry.revolutexchange.network;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    public static final String BASE_URL = "http://api.fixer.io";
    private static final int TIMEOUT = 25;

    @NonNull
    private final Api api;

    private ApiService() {
        api = initRetrofit().create(Api.class);
    }

    @NonNull
    private Retrofit initRetrofit() {
        OkHttpClient.Builder okOkHttpBuilder = new OkHttpClient.Builder();
        OkHttpClient okHttpClient = okOkHttpBuilder.readTimeout(TIMEOUT, TimeUnit.SECONDS)
                                                   .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                                                   .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                                                   .build();

        return new Retrofit.Builder().baseUrl(BASE_URL)
                                     .client(okHttpClient)
                                     .addConverterFactory(GsonConverterFactory.create())
                                     .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                                     .build();
    }

    @NonNull
    public static ApiService getInstance() {
        return ApiServiceHolder.HOLDER_INSTANCE;
    }

    @NonNull
    public Api getApi() {
        return api;
    }

    public static class ApiServiceHolder {
        public static final ApiService HOLDER_INSTANCE = new ApiService();
    }
}
