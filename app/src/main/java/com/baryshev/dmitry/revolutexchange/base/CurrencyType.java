package com.baryshev.dmitry.revolutexchange.base;

import android.support.annotation.NonNull;

import java.util.Currency;
import java.util.Locale;

public enum CurrencyType {
    GBP(Currency.getInstance(Locale.UK).getSymbol()),
    EUR(Currency.getInstance(Locale.GERMANY).getSymbol()),
    USD(Currency.getInstance(Locale.US).getSymbol());

    @NonNull
    private String currency;

    CurrencyType(@NonNull String currency) {
        this.currency = currency;
    }

    @NonNull
    public String getCurrency() {
        return currency;
    }
}
