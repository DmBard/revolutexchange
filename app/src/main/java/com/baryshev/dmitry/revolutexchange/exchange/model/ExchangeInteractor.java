package com.baryshev.dmitry.revolutexchange.exchange.model;

import android.support.annotation.NonNull;

import com.baryshev.dmitry.revolutexchange.base.repos.INetworkRepository;
import com.baryshev.dmitry.revolutexchange.network.pojo.BaseRate;
import com.baryshev.dmitry.revolutexchange.network.pojo.Rates;

import io.reactivex.Flowable;

public class ExchangeInteractor implements IExchangeInteractor {
    @NonNull
    private final INetworkRepository networkRepository;

    public ExchangeInteractor(@NonNull INetworkRepository networkRepository) {
        this.networkRepository = networkRepository;
    }

    @NonNull
    @Override
    public Flowable<Rates> getRates(@NonNull String baseRate) {
        return networkRepository.getBaseRate(baseRate).map(BaseRate::getRates);
    }

}
