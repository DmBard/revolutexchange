package com.baryshev.dmitry.revolutexchange.base;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SchedulerManager implements ISchedulerManager{

    @NonNull
    @Override
    public Scheduler io() {
        return Schedulers.io();
    }

    @NonNull
    @Override
    public Scheduler mainThread() {
        return AndroidSchedulers.mainThread();
    }
}
