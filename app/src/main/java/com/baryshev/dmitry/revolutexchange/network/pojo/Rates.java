package com.baryshev.dmitry.revolutexchange.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rates {
    @SerializedName("AUD")
    @Expose
    private double aud;
    @SerializedName("BGN")
    @Expose
    private double bgn;
    @SerializedName("BRL")
    @Expose
    private double brl;
    @SerializedName("CAD")
    @Expose
    private double cad;
    @SerializedName("CHF")
    @Expose
    private double chf;
    @SerializedName("CNY")
    @Expose
    private double cny;
    @SerializedName("CZK")
    @Expose
    private double czk;
    @SerializedName("DKK")
    @Expose
    private double dkk;
    @SerializedName("GBP")
    @Expose
    private double gbp;
    @SerializedName("HKD")
    @Expose
    private double hkd;
    @SerializedName("HRK")
    @Expose
    private double hrk;
    @SerializedName("HUF")
    @Expose
    private double huf;
    @SerializedName("IDR")
    @Expose
    private double idr;
    @SerializedName("ILS")
    @Expose
    private double ils;
    @SerializedName("INR")
    @Expose
    private double inr;
    @SerializedName("JPY")
    @Expose
    private double jpy;
    @SerializedName("KRW")
    @Expose
    private double krw;
    @SerializedName("MXN")
    @Expose
    private double mxn;
    @SerializedName("MYR")
    @Expose
    private double myr;
    @SerializedName("NOK")
    @Expose
    private double nok;
    @SerializedName("NZD")
    @Expose
    private double nzd;
    @SerializedName("PHP")
    @Expose
    private double php;
    @SerializedName("PLN")
    @Expose
    private double pln;
    @SerializedName("RON")
    @Expose
    private double ron;
    @SerializedName("RUB")
    @Expose
    private double rub;
    @SerializedName("SEK")
    @Expose
    private double sek;
    @SerializedName("SGD")
    @Expose
    private double sgd;
    @SerializedName("THB")
    @Expose
    private double thb;
    @SerializedName("TRY")
    @Expose
    private double tRY;
    @SerializedName("USD")
    @Expose
    private double usd;
    @SerializedName("ZAR")
    @Expose
    private double zar;
    @SerializedName("EUR")
    @Expose
    private double eur;

    public double getAud() {
        return aud;
    }

    public double getBgn() {
        return bgn;
    }

    public double getBrl() {
        return brl;
    }

    public double getCad() {
        return cad;
    }

    public double getChf() {
        return chf;
    }

    public double getCny() {
        return cny;
    }

    public double getCzk() {
        return czk;
    }

    public double getDkk() {
        return dkk;
    }

    public double getGbp() {
        return gbp;
    }

    public double getHkd() {
        return hkd;
    }

    public double getHrk() {
        return hrk;
    }

    public double getHuf() {
        return huf;
    }

    public double getIdr() {
        return idr;
    }

    public double getIls() {
        return ils;
    }

    public double getInr() {
        return inr;
    }

    public double getJpy() {
        return jpy;
    }

    public double getKrw() {
        return krw;
    }

    public double getMxn() {
        return mxn;
    }

    public double getMyr() {
        return myr;
    }

    public double getNok() {
        return nok;
    }

    public double getNzd() {
        return nzd;
    }

    public double getPhp() {
        return php;
    }

    public double getPln() {
        return pln;
    }

    public double getRon() {
        return ron;
    }

    public double getRub() {
        return rub;
    }

    public double getSek() {
        return sek;
    }

    public double getSgd() {
        return sgd;
    }

    public double getThb() {
        return thb;
    }

    public double gettRY() {
        return tRY;
    }

    public double getUsd() {
        return usd;
    }

    public double getZar() {
        return zar;
    }

    public double getEur() {
        return eur;
    }

    public void setGbp(double gbp) {
        this.gbp = gbp;
    }

    public void setUsd(double usd) {
        this.usd = usd;
    }

    public void setEur(double eur) {
        this.eur = eur;
    }
}
