package com.baryshev.dmitry.revolutexchange;

import android.support.annotation.NonNull;

import com.baryshev.dmitry.revolutexchange.base.ISchedulerManager;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class TestSchedulerManager implements ISchedulerManager {
    @NonNull
    @Override
    public Scheduler io() {
        return Schedulers.trampoline();
    }

    @NonNull
    @Override
    public Scheduler mainThread() {
        return Schedulers.trampoline();
    }
}
