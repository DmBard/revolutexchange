package com.baryshev.dmitry.revolutexchange.exchange.presenter;

import com.baryshev.dmitry.revolutexchange.TestSchedulerManager;
import com.baryshev.dmitry.revolutexchange.base.CurrencyType;
import com.baryshev.dmitry.revolutexchange.exchange.model.ExchangeRateVM;
import com.baryshev.dmitry.revolutexchange.exchange.model.IExchangeInteractor;
import com.baryshev.dmitry.revolutexchange.exchange.view.IExchangeView;
import com.baryshev.dmitry.revolutexchange.network.pojo.Rates;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.UnknownHostException;

import io.reactivex.Flowable;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExchangePresenterTest {

    @Mock
    IExchangeView view;
    @Mock
    private IExchangeInteractor exchangeInteractor;

    private ExchangePresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new ExchangePresenter(exchangeInteractor, new TestSchedulerManager());
        presenter.view = view;
        ExchangeRateVM rateVM = new ExchangeRateVM();
        rateVM.setFromCurrencyType(CurrencyType.EUR);
        rateVM.setToCurrencyType(CurrencyType.USD);
        rateVM.setRateKoeff(300);
        rateVM.setFromVal(1);
        rateVM.setToVal(300);

        presenter.rateVM = rateVM;
    }

    @Test
    public void onRateTypeChanged_currencyTypeIsGBPDirectionFrom_success() throws Exception {
        when(exchangeInteractor.getRates(anyString())).thenReturn(Flowable.just(initTestRate()));

        presenter.onRateTypeChanged(CurrencyType.GBP, ExchangePresenter.ExchangeDirection.FROM);

        verify(view).updateToRate(any(ExchangeRateVM.class));
        verify(view, never()).updateFromRate(any());
        verify(view).updateExchangeKoeff(any(ExchangeRateVM.class));
        verify(view, never()).showError(anyInt());
        assertEquals(presenter.rateVM.getRateKoeff(), 200.0);
        assertEquals(presenter.rateVM.getFromCurrencyType(), CurrencyType.GBP);
        assertEquals(presenter.rateVM.getToCurrencyType(), CurrencyType.USD);
        assertEquals(presenter.rateVM.getToVal(), 200.0);
        assertEquals(presenter.rateVM.getFromVal(), 1.0);
    }

    @Test
    public void onRateTypeChanged_currencyTypeIsGBPDirectionTo_success() throws Exception {
        when(exchangeInteractor.getRates(anyString())).thenReturn(Flowable.just(initTestRate()));

        presenter.onRateTypeChanged(CurrencyType.GBP, ExchangePresenter.ExchangeDirection.TO);

        verify(view).updateToRate(any(ExchangeRateVM.class));
        verify(view, never()).updateFromRate(any());
        verify(view).updateExchangeKoeff(any(ExchangeRateVM.class));
        verify(view, never()).showError(anyInt());
        assertEquals(presenter.rateVM.getRateKoeff(), 100.0);
        assertEquals(presenter.rateVM.getFromCurrencyType(), CurrencyType.EUR);
        assertEquals(presenter.rateVM.getToCurrencyType(), CurrencyType.GBP);
        assertEquals(presenter.rateVM.getToVal(), 100.0);
        assertEquals(presenter.rateVM.getFromVal(), 1.0);
    }

    @Test
    public void onRateValueChanged_correctValueAndDirectionTo_success() throws Exception {
        when(exchangeInteractor.getRates(anyString())).thenReturn(Flowable.just(initTestRate()));

        presenter.onRateValueChanged("600", ExchangePresenter.ExchangeDirection.TO);

        verify(view, never()).updateToRate(any(ExchangeRateVM.class));
        verify(view).updateFromRate(any(ExchangeRateVM.class));
        verify(view).updateExchangeKoeff(any(ExchangeRateVM.class));
        verify(view, never()).showError(anyInt());
        assertEquals(presenter.rateVM.getRateKoeff(), 200.0);
        assertEquals(presenter.rateVM.getFromCurrencyType(), CurrencyType.EUR);
        assertEquals(presenter.rateVM.getToCurrencyType(), CurrencyType.USD);
        assertEquals(presenter.rateVM.getToVal(), 600.0);
        assertEquals(presenter.rateVM.getFromVal(), 3.0);
    }

    @Test
    public void onRateValueChanged_correctValueAndDirectionFrom_success() throws Exception {
        when(exchangeInteractor.getRates(anyString())).thenReturn(Flowable.just(initTestRate()));

        presenter.onRateValueChanged("2", ExchangePresenter.ExchangeDirection.FROM);

        verify(view).updateToRate(any(ExchangeRateVM.class));
        verify(view, never()).updateFromRate(any(ExchangeRateVM.class));
        verify(view).updateExchangeKoeff(any(ExchangeRateVM.class));
        verify(view, never()).showError(anyInt());
        assertEquals(presenter.rateVM.getRateKoeff(), 200.0);
        assertEquals(presenter.rateVM.getFromCurrencyType(), CurrencyType.EUR);
        assertEquals(presenter.rateVM.getToCurrencyType(), CurrencyType.USD);
        assertEquals(presenter.rateVM.getToVal(), 400.0);
        assertEquals(presenter.rateVM.getFromVal(), 2.0);
    }

    @Test
    public void onRateValueChanged_incorrectValueAndDirectionFrom_viewModelIsNotChanged() throws Exception {
        when(exchangeInteractor.getRates(anyString())).thenReturn(Flowable.just(initTestRate()));

        presenter.onRateValueChanged("test", ExchangePresenter.ExchangeDirection.FROM);

        verify(view, never()).updateToRate(any(ExchangeRateVM.class));
        verify(view).updateFromRate(any(ExchangeRateVM.class));
        verify(view, never()).updateExchangeKoeff(any(ExchangeRateVM.class));
        verify(view, never()).showError(anyInt());
        assertEquals(presenter.rateVM.getRateKoeff(), 300.0);
        assertEquals(presenter.rateVM.getFromCurrencyType(), CurrencyType.EUR);
        assertEquals(presenter.rateVM.getToCurrencyType(), CurrencyType.USD);
        assertEquals(presenter.rateVM.getToVal(), 300.0);
        assertEquals(presenter.rateVM.getFromVal(), 1.0);
    }

    @Test
    public void onDetach() throws Exception {
        presenter.onDetach();
        assertTrue(presenter.view == IExchangeView.EMPTY);
    }

    @Test
    public void onInit_throwException_showError() throws Exception {
        when(exchangeInteractor.getRates(anyString())).thenThrow(UnknownHostException.class);

        presenter.onInit(view, CurrencyType.EUR, CurrencyType.USD);

        verify(view, never()).updateToRate(any());
        verify(view, never()).updateFromRate(any());
        verify(view, never()).updateExchangeKoeff(any());
        verify(view).showError(anyInt());
    }

    private Rates initTestRate() {
        Rates rates = new Rates();
        rates.setGbp(100);
        rates.setUsd(200);
        rates.setEur(1);
        return rates;
    }
}